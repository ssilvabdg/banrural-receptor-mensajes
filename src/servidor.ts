import express, { Express } from 'express';
import cors from 'cors';
import { createServer, Server as ServerHttp } from 'http';
import { Server as ServerIo } from 'socket.io';
import { listaSocket } from './routes/socket.route';
import https from 'https';
export class Servidor {
    private app: Express;
    private port: string;
    //private logLevel: string;
    //private fileLogCont: string;
    private httpServer: ServerHttp;
    private serverIo: ServerIo;
   // private dirlog:string;

    constructor() {
        //var fs = require('fs');
        //var privateKey = fs.readFileSync('./SSL/wildcard_yilport_com.key').toString(); 
        //var certificate = fs.readFileSync('./SSL/wildcard_yilport_com.crt').toString(); 
        //var ca = fs.readFileSync('./SSL/CACert.crt').toString();

        this.app = express();
        this.port = `${process.env.PORT}`;
        //this.logLevel = `${process.env.LogLevel}`;
        //this.fileLogCont = `${process.env.FileLogCont}`;
       // this.dirlog = 'C:/Users/BDGSA/Desktop/logF/';
        this.httpServer = createServer( this.app );
        //this.httpServer = https.createServer({key:privateKey,cert:certificate,ca:ca }, this.app);
        this.serverIo = new ServerIo( this.httpServer, {
            cors: {
                origin: '*'
            }
        } );
        
        // Se guarda socketIo dentro de express request para usarlo en api rest
        this.app.set('socketIo', this.serverIo);
        
       // var log4js = require('log4js');

//         var hoy = new Date();
//         var fecha_actual = String(hoy.getFullYear()+"-"+(hoy.getMonth()+1)+"-"+hoy.getDate());
    
//         var config1=  {
//             appenders: { All1Logs: {type: "dateFile", filename: `${this.dirlog+"todos_"+this.logLevel+"_"+fecha_actual+".log"}`, pattern : "-yyyy-MM-dd", layout: { type: 'pattern', pattern: '[%d][%m] [%p][%c] %n' }     }},
//             categories: { default: { appenders: [ "All1Logs" ], level: "ALL" }}            }
//         var config2={
//         appenders: { correcto: { type: "dateFile", filename: `${this.dirlog+"correcto_"+this.logLevel+"_"+fecha_actual+".log"}`, pattern : "-yyyy-MM-dd", layout: { type: 'pattern', pattern: '[%d][%m] [%p][%c] %n' }},
//             incorrecto_error: { type: "dateFile", filename: `${this.dirlog+"incorrecto_"+this.logLevel+"_"+fecha_actual+".log"}`, pattern : "-yyyy-MM-dd", layout: { type: 'pattern', pattern: '[%d][%m] [%p][%c] %n' }},
//             exito: { type: 'logLevelFilter', appender: './log/exito', level : 'info' },   },
//             categories: {  
//             ENVIO_MENSAJE_CORRECTO: { appenders: ['correcto'], level: 'info' },
//             ENVIO_MENSAJE_INCORRECTO: { appenders: ['incorrecto_error'], level: 'error'   },
//             default: { appenders: ['incorrecto_error'], level: 'error' }}       }
           
//         var config3={
//         appenders: {  correcto: { type: "dateFile", filename: `${this.dirlog+"correcto_"+this.logLevel+"_"+fecha_actual+".log"}`, pattern : "-yyyy-MM-dd", layout: { type: 'pattern', pattern: '[%d][%m] [%p][%c] %n' }},
//             incorrecto_error: { type: "dateFile", filename: `${this.dirlog+"incorrecto_"+this.logLevel+"_"+fecha_actual+".log"}`, pattern : "-yyyy-MM-dd", layout: { type: 'pattern', pattern: '[%d][%m] [%p][%c] %n' }},
//             tError: { type: "dateFile", filename: `${this.dirlog+"todos_"+this.logLevel+"_"+fecha_actual+".log"}`, pattern : "-yyyy-MM-dd", layout: { type: 'pattern', pattern: '[%d][%m] [%p][%c] %n' }},
//             filtrado: { type: 'logLevelFilter', appender: 'tError', level : 'error' },  },
//          categories: {  
//             ENVIO_MENSAJE_CORRECTO: { appenders: ['correcto'], level: 'info' },
//             ENVIO_MENSAJE_INCORRECTO: { appenders: ['incorrecto_error'], level: 'error'   },
//             default: { appenders: ['filtrado'], level: 'error' }}  }



//         if(this.fileLogCont === '1'){
//             //SE CREA UN UNICO ARCHIVO DE LOG
//             console.log("filelogcont = 1 - loglevel = any");
//             log4js.configure(config1);     
//             const logger = log4js.getLogger();  
//             this.app.use(log4js.connectLogger(logger, { level: 'auto' }));

//         } else if (this.fileLogCont === '2'){
//             if(this.logLevel === 'All'){
//                 //SE CREA UN ARCHIVO PARA EXITOSOS Y OTRO NO EXITOSOS JUNTO A ERRORES
//                 console.log("filelogcont = 2 - loglevel = all");
//                 log4js.configure(config2);     
//                 const logger = log4js.getLogger('ENVIO_MENSAJE_INCORRECTO');  
//                 this.app.use(log4js.connectLogger(logger, { level: 'auto' }));
//             } else {
//                 //SE CREA UN UNICO ARCHIVO DE LOG
//                 console.log("filelogcont = 2 - loglevel = any");
//                 log4js.configure(config1);     
//                 const logger = log4js.getLogger();  
//                 this.app.use(log4js.connectLogger(logger, { level: 'auto' }));
//             }
//         } else if (this.fileLogCont === '3'){
//             if(this.logLevel === 'All'){
//                 //SE CREA UN ARCHIVO PARA EXITOSOS, OTRO NO EXITOSOS Y OTRO PARA ERRORES
//                 console.log("filelogcont = 3 - loglevel = all");
//                 log4js.configure(config3);     
//                 const logger = log4js.getLogger();  
//                 this.app.use(log4js.connectLogger(logger, { level: 'auto' }));
//               } else {
//                 //SE CREA UN UNICO ARCHIVO DE LOG
//                 console.log("filelogcont = 3 - loglevel = any");
//                 log4js.configure(config1);     
//                 const logger = log4js.getLogger();  
//                 this.app.use(log4js.connectLogger(logger, { level: 'auto' }));
//             }
//         }else  {
//             log4js.configure(config1);     
//             const logger = log4js.getLogger();  
//             this.app.use(log4js.connectLogger(logger, { level: 'auto' }));

//         }
        
//  ;
        this.configureServer().then().catch(err => {
            console.log('No se ha iniciado debido al error: ', err);
        })
    }

    async configureServer() {
        await this.middlewares();
        await this.routes();
        await this.sockets();
        await this.start();
    }

    async middlewares() {
        this.app.use(express.json({limit: '50mb'}));
        this.app.use(express.urlencoded({limit:'50mb'}))
        this.app.use(cors());
    }

    async routes() {
        this.app.use('/enviar-mensaje', require('./routes/mensaje.route'));
        this.app.use('/bienvenida', require('./routes/mensaje.route'));
    }

    async sockets() {
        listaSocket.forEach( el => {
            this.serverIo.on(el.evento, el.funcion);
        });
    }

    async start() {
        this.httpServer.listen(this.port, () => {
            return console.log(`server is listening on ${this.port}`);
        });
    }

}