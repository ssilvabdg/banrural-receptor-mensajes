import { Socket } from "socket.io"
import { Encript } from '../encript'
import logCtrl from './log.controller'

var component = logCtrl.component;
var status = logCtrl.status;

const connection = (socket: Socket) => {
    //SE OBTIENE EL SOCKET ID
    const idSocket = socket.id;

    //SE DECLARAN VARIABLES
    var idSocket2Aux: any;
    var idErrorAux: any;
    const axios = require('axios');
    const varEncriptado = `${process.env.Encriptado}`;
    const desencriptar = new Encript();

    //SE CREAN LAS CADENAS DEL SOCKET CORRECTO PARA DATOS ENCRIPTADOS Y NO ENCRIPTADOS
    const armado = `{\"message\":{\"sender\": {\"socket\":\"${idSocket}\"}}}`
    const armado2 = `{\"sender\": {\"socket\":\"${idSocket}\"}}`
    const idSocket2 = JSON.parse(armado);
    const idSocket22 = JSON.parse(armado2);

     //SE CREAN LAS CADENAS DEL SOCKET INCORRECTO PARA DATOS ENCRIPTADOS Y NO ENCRIPTADOS
    const armadoE = `{\"message\":{\"sender\": {\"socket\":\"${idSocket}\",\"error\":\"S\"}}}`
    const armadoE2 = `{\"sender\": {\"socket\":\"${idSocket}\",\"error\":\"S\"}}`
    const idError = JSON.parse(armadoE);
    const idError2 = JSON.parse(armadoE2);
    
    //COMIENZA METODO AUTH
    socket.on('auth', (id: string) => {
        console.log("ENTRE EN AUTH Y ESTE ES EL ID ", id);
        //SE SEPARA POR MEDIO DE SPLIT "#" PARA CREAR OBTENER EL USUARIO Y EL ID DISPOSITIVO
        var separacion = id.split("#");
        var pUsuario = separacion[1];
        var pidDispositivo = separacion[0];

        //SE CREA CONSTANTE DATA CON EL USUARIO Y EL ID DISPOSITIVO
        const data = {
            pUsuario: pUsuario,
            pIdDispositivo: pidDispositivo
        };

        //SI VIENE "S" QUIERE DECIR QUE ES LA PRIMERA VEZ QUE SE CONECTA EL DISPOSITIVO Y SE DEBEN ELIMINAR MENSAJES ENCOLADOS
        if (separacion[2] == "S") {

            //INTENTA ELIMINAR LOS MENSAJES ENCOLADOS EN CASO DE EXISTIR
            var capamedia = require('../middlewares/capamedia')();
            capamedia.consultarBanrural(data,853)
                .then((reso: any) => {
                    reso=reso.eMensaje[0];
                   // reso=reso.data;
                   console.log("esto devuelve al eliminar",reso);
                    if (reso.pCodigo == 0) {
                        console.log("MENSAJES ELIMINADOS EN PRIMERA CONEXION");
                        socket.join("dispositivosConectados");
                        //OBTIENE EL SOCKET EN FORMATO JSON SIN ENCRIPTAR
                        idSocket2Aux = idSocket2;
                        //SE DECIDE SI SE ENCRIPTAN O NO LOS DATOS DEL SOCKET EN FORMATO JSON
                        if (varEncriptado === '1') {
                            idSocket2Aux = desencriptar.codifi(idSocket2);
                            socket.emit(`mensaje-${separacion[0]}`, { d: idSocket2Aux })
                        } else {
                            socket.emit(`mensaje-${separacion[0]}`, { message: idSocket22 })
                        }
                    } else {
                        if (reso.pCodigo == 4) {
                            console.log("NO HABIAN MENSAJES POR ELIMINAR EN PRIMERA CONEXION");
                            socket.join("dispositivosConectados");
                            //OBTIENE EL SOCKET EN FORMATO JSON SIN ENCRIPTAR
                            idSocket2Aux = idSocket2;
                            //SE DECIDE SI SE ENCRIPTAN O NO LOS DATOS DEL SOCKET EN FORMATO JSON
                            if (varEncriptado === '1') {
                                idSocket2Aux = desencriptar.codifi(idSocket2);
                                socket.emit(`mensaje-${separacion[0]}`, { d: idSocket2Aux })
                            } else {
                                socket.emit(`mensaje-${separacion[0]}`, { message: idSocket22 })
                            }
                        } else {
                            console.log("QUIERE DECIR QUE OCURRIO CUALQUIER OTRO ERROR");
                            //NO SE ENVIA NADA COMO MUESTRA DE ERROR Y SE QUEDA COLGADO EN EL LOADER
                        }
                    }
                }).catch((err: any) => {
                    //NO SE ENVIA NADA COMO MUESTRA DE ERROR Y SE QUEDA COLGADO EN EL LOADER
                    console.log("SE PRESENTO EL SIGUIENTE ERROR AL MOMENTO DE ELIMINAR LOS MENSAJES AL INICIO", err);
                    logCtrl.escribirBitacoraLog(pUsuario, component.apiCola, "ERROR AL ELIMINAR MENSAJES EN PRIMERA CONEXION", status.fallido);
                });
        //SI ENTRA ACA DEBE DE SER POR UNA RECONEXION
        } else { 
            var capamedia = require('../middlewares/capamedia')();
            capamedia.consultarBanrural(data,852)
                .then((res: any) => {
                   // console.log("Esto devuelveeeeee",res);
                   // res=res.data;
                    //BORRAR MENSAJES
                    var mensajes: any = "";
                    var i = 0;
                    var aux: any = "";
                    var aux2: any = "";
                    if (res.eMensaje[0].pCodigo == 0) {
                        console.log("RECONEXION - HAY ELEMENTOS EN LA COLA");
                        var capamedia = require('../middlewares/capamedia')();
                        capamedia.consultarBanrural(data,853)
                            .then((reso: any) => {
                                //reso=reso.data;
                                if (reso.eMensaje[0].pCodigo == 0) {
                                    console.log("QUIERE DECIR QUE ELIMINO MENSAJES");
                                    console.log("SE BORRAN LOS MENSAJES ENVIADOS");
                                    if (res.eMensaje[0].pCodigo == 0) {
                                        console.log("TRAIGO DATA");
                                        //console.log("DATA", res.data);
                                        console.log('EMITIENDO EVENTO', `mensaje-${separacion[0]}`);
                                        socket.join("dispositivosConectados");
                                        idSocket2Aux = idSocket2;
                                        if (varEncriptado === '1') {
                                            idSocket2Aux = desencriptar.codifi(idSocket2);
                                            socket.emit(`mensaje-${separacion[0]}`, { d: idSocket2Aux })
                                        } else {
                                            socket.emit(`mensaje-${separacion[0]}`, { message: idSocket22 })
                                        }
                                        mensajes = res.eDetalle[0];
                                        //mensajes = res.data.detalle;
                                        console.log("SE BORRAN LOS MENSAJES ENVIADOS");
                                        while (i < mensajes.length) {
                                            try {
                                                //ENVIAR MENSAJES REZAGADOS
                                                if (varEncriptado === '1') {
                                                    aux2 = desencriptar.decodifi(mensajes[i].Mensaje);
                                                    aux2.sender.socket = socket.id;
                                                    aux2 = JSON.stringify(aux2)
                                                    aux2 = `{\"message\":${aux2}}`
                                                    aux2 = JSON.parse(aux2);
                                                    aux2 = desencriptar.codifi(aux2);
                                                    socket.emit(`mensaje-${separacion[0]}`, { d: aux2 })
                                                } else {
                                                    aux = Buffer.from(mensajes[i].Mensaje, 'base64').toString();
                                                    aux2 = JSON.parse(aux);
                                                    aux2.sender.socket = socket.id;
                                                    socket.emit(`mensaje-${separacion[0]}`, { message: aux2 })
                                                }
                                                console.log("**************************************************************************************************");
                                                console.log("MENSAJE ENVIADO CORRECTAMENTE ", aux2);
                                                console.log("**************************************************************************************************");
                                                i = i + 1;
                                            } catch (error) {
                                                console.log("FALLO AL REENVIAR EL MENSAJE CON ID ", mensajes[i].Id);
                                                logCtrl.escribirBitacoraLog(pUsuario, component.apiCola, `FALLO AL REENVIAR EL MENSAJE CON ID: ${mensajes[i].Id}`, status.fallido);
                                                idErrorAux = idError;
                                                if (varEncriptado === '1') {
                                                    idErrorAux = desencriptar.codifi(idError);
                                                    socket.emit(`mensaje-${separacion[0]}`, { d: idErrorAux })
                                                } else {
                                                    socket.emit(`mensaje-${separacion[0]}`, { message: idError2 })
                                                }
                                                //SE ENVIA MENSAJE DE ERROR VUELVE A INTENTAR
                                                return;
                                            }
                                        }
                                    } else {
                                        //console.log("NO TRAIGO DATA");
                                        //console.log('EMITIENDO EVENTO', `mensaje-${separacion[0]}`);
                                        //socket.join("dispositivosConectados");
                                        //socket.emit(`mensaje-${separacion[0]}`, { message: idSocket2 })
                                    }
                                } else {
                                    if (reso.eMensaje[0].pCodigo == 4) {
                                        //NO SE ENVIA NADA COMO MUESTRA DE ERROR Y SE QUEDA COLGADO EN EL LOADER - CASO IMPOSIBLE
                                        console.log("QUIERE DECIR QUE NO HABIAN MENSAJES POR ELIMINAR");

                                    } else {
                                        //NO SE ENVIA NADA COMO MUESTRA DE ERROR Y SE QUEDA COLGADO EN EL LOADER - CASO IMPOSIBLE
                                        console.log("QUIERE DECIR QUE OCURRIO CUALQUIER OTRO ERROR AL ELIMINAR");
                                    }
                                }
                            }).catch((err: any) => {
                                //NO SE ENVIA NADA COMO MUESTRA DE ERROR Y SE QUEDA COLGADO EN EL LOADER
                                console.log("SE PRESENTO EL SIGUIENTE ERROR AL MOMENTO DE ELIMINAR LOS MENSAJES REZAGADOS", err);
                                logCtrl.escribirBitacoraLog(pUsuario, component.apiCola, "ERROR AL ELIMINAR MENSAJES REZAGADOS", status.fallido);
                            });
                    } else {
                        if (res.eMensaje[0].pCodigo == 4) {
                            console.log("QUIERE DECIR QUE NO HAY MENSAJES EN LA COLA");
                            //NO HAY MENSAJES Y HAY QUE MANDAR SOCKET
                            console.log('EMITIENDO EVENTO', `mensaje-${separacion[0]}`);
                            socket.join("dispositivosConectados");
                            idSocket2Aux = idSocket2;
                            if (varEncriptado === '1') {
                                idSocket2Aux = desencriptar.codifi(idSocket2);
                                socket.emit(`mensaje-${separacion[0]}`, { d: idSocket2Aux })
                            } else {
                                socket.emit(`mensaje-${separacion[0]}`, { message: idSocket22 })
                            }
                        } else {
                            //NO SE ENVIA NADA COMO MUESTRA DE ERROR Y SE QUEDA COLGADO EN EL LOADER
                            console.log("OCURRIO CUALQUIER OTRO ERROR OBTENIENDO LOS MENSAJES DE LA COLA");
                        }
                    }
                }).catch((err: any) => {
                    //NO SE ENVIA NADA COMO MUESTRA DE ERROR Y SE QUEDA COLGADO EN EL LOADER
                    logCtrl.escribirBitacoraLog(pUsuario, component.apiCola, "ERROR AL CONSULTAR MENSAJES EN COLA", status.fallido);
                    console.log("OCURRIO UN ERROR CONSULTANDO LOS MENSAJES EN COLA");
                    console.error(err);
                });
        }

    });


    socket.on('desconectar', () => {
        try {
            socket.leave("dispositivosConectados");
            socket.disconnect();
            console.log(`EL USUARIO ${idSocket} SE HA DESCONECTADO`);
        } catch (error) {
            console.log(`RECIBIENDO PETICION DE DESCONEXION DE ${idSocket}`);
        }
    });


    socket.on('conectado', (idSocket: string) => {

        const sockets = socket.in("dispositivosConectados").allSockets();
        var conectado = false;
        for (let i in sockets) {
            if (i == idSocket) {
                conectado = true;
            }
        }

        if (conectado) {
            console.log("CONECTADO");
        } else {
            console.log("DESCONECTADO");
        }

    });


}


/*const parapruebas = () => {
    // socket.emit('disconnect', { message: '¡Hasta luego Amigo! 😀' } );
    console.log('Mensaje de prueba');
}*/

export const SocketCtrl = {
    connection
    //parapruebas
}