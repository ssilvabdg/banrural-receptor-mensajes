const escribirBitacoraLog=(usuario:string,componente:component,descripcion:string,status:status)=>{
    const axios = require('axios');
    const fechaActual= new Date();
    var data={
    pUsuario:usuario,
    pFechaHora:fechaActual,
    pComponente:componente,
    pDescripcion:descripcion,
    pStatus:status
    }
    console.log("datos enviados",data);
    var capamedia = require('../middlewares/capamedia')();
    capamedia.consultarBanrural(data,855).then((res:any) => {
    console.log("SE GUARDO EL REGISTRO EN LA BITACORA",res);
}).catch((err:any) => {
    console.log("OCURRIO UN PROBLEMA INSERTANDO EL REGISTRO EN LA BITACORA", err);
});

}

enum component{
    socket = "socket IO",
    iaISOFT = "IA ISOFT",
    apiReceptor="Api receptor",
    apiCola="API cola de mensajes"
  }

enum status{
    exitoso="200",
    fallido="400"
}  

const log={escribirBitacoraLog,component,status}
export default log;