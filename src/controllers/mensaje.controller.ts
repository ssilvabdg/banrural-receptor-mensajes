import { Request, Response } from "express";
import { Socket } from "socket.io";
import { Encript } from '../encript'
import logCtrl from './log.controller';

var component=logCtrl.component;
var status=logCtrl.status;

const mensaje = async(req: Request, res: Response) => {
    const socket: Socket = req.app.get('socketIo');
    const axios = require('axios');
    const desencriptar = new Encript();
    const varEncriptado = `${process.env.Encriptado}`;
    var reqEntrante = req.body;
    var auxEnvio:any;
    try {
        console.log("**************************************MENSAJE ENTRANTE************************************* ");
        console.log(req.body);
        console.log("******************************************************************************************* ");
        if(varEncriptado==='1'){
            reqEntrante = desencriptar.decodifi(req.body.d);
        }
        const { sender } = reqEntrante;
        console.log("ESTE ES EL BODYS", req.body);
        console.log("ESTE ES EL REQENTRANTE ", reqEntrante);
        console.log("ESTE ES EL REQENTRANTE ID ", reqEntrante.id)
        var userTemp = reqEntrante.id.split("#");
        var pUsuario=userTemp[0];
        const sockets = await socket.in("dispositivosConectados").allSockets();
        var conectado = false;

        sockets.forEach(element => {
            if(sender.socket == element){
                conectado = true;
            } 
        });

        if(conectado){

            res.status(200).json({
                codigo: 0,
                mensaje: `Usted identifico al usuario ${sender.recipient} y reenvio la peticion`
            });

            if(varEncriptado==='1'){
                auxEnvio = JSON.stringify(reqEntrante)
                auxEnvio = `{\"message\":${auxEnvio}}`
                auxEnvio = JSON.parse(auxEnvio);
                auxEnvio = desencriptar.codifi(auxEnvio);
                socket.emit(`mensaje-${sender.recipient}`, { d: auxEnvio });
            } else {
                socket.emit(`mensaje-${sender.recipient}`, { message: req.body });
            }
            console.log("**************************************MENSAJE ENVIADO************************************** ");
            console.log(req.body);
            console.log("******************************************************************************************* ");

        } else {
            res.status(400).json({
                codigo: 4,
                mensaje: `El usuario no se encuentra conectado`
            });
            logCtrl.escribirBitacoraLog(pUsuario,component.socket,`NO SE PUDO IDENTIFICAR AL USUARIO ${sender.socket}`,status.fallido);
            console.log("NO SE PUDO IDENTIFICAR AL USUARIO QUE SE ENVIO COMO CONECTADO");
            var data: any = {}
            var aux2 = sender.id.split("#");
            if(varEncriptado === '1'){
                auxEnvio = JSON.stringify(reqEntrante)
                auxEnvio = `{\"message\":${auxEnvio}}`
                auxEnvio = JSON.parse(auxEnvio);
                auxEnvio = desencriptar.codifi(auxEnvio);
                data = {
                    pUsuario: aux2[0],
                    pIdDispositivo: sender.recipient,
                    pMensajes: req.body.d
                };
            } else {
                const encode = Buffer.from(JSON.stringify(req.body)).toString('base64');
                data = {
                    pUsuario: aux2[0],
                    pIdDispositivo: sender.recipient,
                    pMensajes: encode
                };
            }
           

            try {

                var capamedia = require('../middlewares/capamedia')();
                //console.log("ESTE ES EL DATA", data);
                capamedia.consultarBanrural(data,851).then((res:any)=>{
                if(res.eMensaje[0].pCodigo == 0){
                    console.log("EL MENSAJE SE HA GUARDADO EN LA COLA DE MENSAJES");
                    logCtrl.escribirBitacoraLog(pUsuario,component.apiCola,"EL MENSAJE SE HA GUARDADO EN LA COLA DE MENSAJES",status.exitoso);
                } else {
                    console.log("NO SE PUDO GUARDAR EL MENSAJE EN LA COLA DE MENSAJES");
                    logCtrl.escribirBitacoraLog(pUsuario,component.apiCola,"NO SE PUDO GUARDAR EL MENSAJE EN LA COLA DE MENSAJES RECHAZADO POR SERVIDOR",status.fallido);
                }
                }).catch((err:any) => {
                    console.log("NO SE PUDO GUARDAR EL MENSAJE EN LA COLA DE MENSAJES DEBIDO A ", err);
                    logCtrl.escribirBitacoraLog(pUsuario,component.apiCola,"NO SE PUDO GUARDAR EL MENSAJE EN LA COLA DE MENSAJES DEBIDO A UN ERROR",status.fallido);
                });
            } catch (err) {
                console.log("NO SE PUDO GUARDAR EL MENSAJE EN LA COLA DE MENSAJES DEBIDO A ", err);
                logCtrl.escribirBitacoraLog(pUsuario,component.apiCola,"NO SE PUDO GUARDAR EL MENSAJE EN LA COLA DE MENSAJES DEBIDO A UNA EXCEPCIÓN ",status.fallido);
            }
 
        }
        
    
    } catch(err) {
        res.status(400).json({
            codigo: 4,
            mensaje: `No se pudo realizar el reenvio de la peticion`
        });
        logCtrl.escribirBitacoraLog(pUsuario,component.socket,"OCURRIO UNA EXCEPCIÓN AL INTENTAR REENVIAR EL MENSAJE",status.fallido);
        console.log('OCURRIO EL SIGUIENTE ERROR AL MOMENTO DE REENVIAR EL MENSAJE ' , err);
    }
}



const mensaje2 = async(req: Request, res: Response) => {

    try {
        
        res.status(200).json({
            codigo: 0,
            mensaje: `Bienvenido al receptor de mensajes`
        });
        
    } catch(err) {
        res.status(400).json({
            codigo: 4,
            mensaje: `Ocurrio un error enviando la bienvenida`
        });
    }
}


const Ctrl = {
    mensaje,
    mensaje2
}

export default Ctrl;