import { Router } from 'express';
import Ctrl from '../controllers/mensaje.controller';

const router = Router();

router.post('/', Ctrl.mensaje);

router.get('/', Ctrl.mensaje2);


module.exports = router;