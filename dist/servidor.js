"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Servidor = void 0;
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const http_1 = require("http");
const socket_io_1 = require("socket.io");
const socket_route_1 = require("./routes/socket.route");
class Servidor {
    // private dirlog:string;
    constructor() {
        //var fs = require('fs');
        //var privateKey = fs.readFileSync('./SSL/wildcard_yilport_com.key').toString(); 
        //var certificate = fs.readFileSync('./SSL/wildcard_yilport_com.crt').toString(); 
        //var ca = fs.readFileSync('./SSL/CACert.crt').toString();
        this.app = (0, express_1.default)();
        this.port = `${process.env.PORT}`;
        //this.logLevel = `${process.env.LogLevel}`;
        //this.fileLogCont = `${process.env.FileLogCont}`;
        // this.dirlog = 'C:/Users/BDGSA/Desktop/logF/';
        this.httpServer = (0, http_1.createServer)(this.app);
        //this.httpServer = https.createServer({key:privateKey,cert:certificate,ca:ca }, this.app);
        this.serverIo = new socket_io_1.Server(this.httpServer, {
            cors: {
                origin: '*'
            }
        });
        // Se guarda socketIo dentro de express request para usarlo en api rest
        this.app.set('socketIo', this.serverIo);
        // var log4js = require('log4js');
        //         var hoy = new Date();
        //         var fecha_actual = String(hoy.getFullYear()+"-"+(hoy.getMonth()+1)+"-"+hoy.getDate());
        //         var config1=  {
        //             appenders: { All1Logs: {type: "dateFile", filename: `${this.dirlog+"todos_"+this.logLevel+"_"+fecha_actual+".log"}`, pattern : "-yyyy-MM-dd", layout: { type: 'pattern', pattern: '[%d][%m] [%p][%c] %n' }     }},
        //             categories: { default: { appenders: [ "All1Logs" ], level: "ALL" }}            }
        //         var config2={
        //         appenders: { correcto: { type: "dateFile", filename: `${this.dirlog+"correcto_"+this.logLevel+"_"+fecha_actual+".log"}`, pattern : "-yyyy-MM-dd", layout: { type: 'pattern', pattern: '[%d][%m] [%p][%c] %n' }},
        //             incorrecto_error: { type: "dateFile", filename: `${this.dirlog+"incorrecto_"+this.logLevel+"_"+fecha_actual+".log"}`, pattern : "-yyyy-MM-dd", layout: { type: 'pattern', pattern: '[%d][%m] [%p][%c] %n' }},
        //             exito: { type: 'logLevelFilter', appender: './log/exito', level : 'info' },   },
        //             categories: {  
        //             ENVIO_MENSAJE_CORRECTO: { appenders: ['correcto'], level: 'info' },
        //             ENVIO_MENSAJE_INCORRECTO: { appenders: ['incorrecto_error'], level: 'error'   },
        //             default: { appenders: ['incorrecto_error'], level: 'error' }}       }
        //         var config3={
        //         appenders: {  correcto: { type: "dateFile", filename: `${this.dirlog+"correcto_"+this.logLevel+"_"+fecha_actual+".log"}`, pattern : "-yyyy-MM-dd", layout: { type: 'pattern', pattern: '[%d][%m] [%p][%c] %n' }},
        //             incorrecto_error: { type: "dateFile", filename: `${this.dirlog+"incorrecto_"+this.logLevel+"_"+fecha_actual+".log"}`, pattern : "-yyyy-MM-dd", layout: { type: 'pattern', pattern: '[%d][%m] [%p][%c] %n' }},
        //             tError: { type: "dateFile", filename: `${this.dirlog+"todos_"+this.logLevel+"_"+fecha_actual+".log"}`, pattern : "-yyyy-MM-dd", layout: { type: 'pattern', pattern: '[%d][%m] [%p][%c] %n' }},
        //             filtrado: { type: 'logLevelFilter', appender: 'tError', level : 'error' },  },
        //          categories: {  
        //             ENVIO_MENSAJE_CORRECTO: { appenders: ['correcto'], level: 'info' },
        //             ENVIO_MENSAJE_INCORRECTO: { appenders: ['incorrecto_error'], level: 'error'   },
        //             default: { appenders: ['filtrado'], level: 'error' }}  }
        //         if(this.fileLogCont === '1'){
        //             //SE CREA UN UNICO ARCHIVO DE LOG
        //             console.log("filelogcont = 1 - loglevel = any");
        //             log4js.configure(config1);     
        //             const logger = log4js.getLogger();  
        //             this.app.use(log4js.connectLogger(logger, { level: 'auto' }));
        //         } else if (this.fileLogCont === '2'){
        //             if(this.logLevel === 'All'){
        //                 //SE CREA UN ARCHIVO PARA EXITOSOS Y OTRO NO EXITOSOS JUNTO A ERRORES
        //                 console.log("filelogcont = 2 - loglevel = all");
        //                 log4js.configure(config2);     
        //                 const logger = log4js.getLogger('ENVIO_MENSAJE_INCORRECTO');  
        //                 this.app.use(log4js.connectLogger(logger, { level: 'auto' }));
        //             } else {
        //                 //SE CREA UN UNICO ARCHIVO DE LOG
        //                 console.log("filelogcont = 2 - loglevel = any");
        //                 log4js.configure(config1);     
        //                 const logger = log4js.getLogger();  
        //                 this.app.use(log4js.connectLogger(logger, { level: 'auto' }));
        //             }
        //         } else if (this.fileLogCont === '3'){
        //             if(this.logLevel === 'All'){
        //                 //SE CREA UN ARCHIVO PARA EXITOSOS, OTRO NO EXITOSOS Y OTRO PARA ERRORES
        //                 console.log("filelogcont = 3 - loglevel = all");
        //                 log4js.configure(config3);     
        //                 const logger = log4js.getLogger();  
        //                 this.app.use(log4js.connectLogger(logger, { level: 'auto' }));
        //               } else {
        //                 //SE CREA UN UNICO ARCHIVO DE LOG
        //                 console.log("filelogcont = 3 - loglevel = any");
        //                 log4js.configure(config1);     
        //                 const logger = log4js.getLogger();  
        //                 this.app.use(log4js.connectLogger(logger, { level: 'auto' }));
        //             }
        //         }else  {
        //             log4js.configure(config1);     
        //             const logger = log4js.getLogger();  
        //             this.app.use(log4js.connectLogger(logger, { level: 'auto' }));
        //         }
        //  ;
        this.configureServer().then().catch(err => {
            console.log('No se ha iniciado debido al error: ', err);
        });
    }
    configureServer() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.middlewares();
            yield this.routes();
            yield this.sockets();
            yield this.start();
        });
    }
    middlewares() {
        return __awaiter(this, void 0, void 0, function* () {
            this.app.use(express_1.default.json({ limit: '50mb' }));
            this.app.use(express_1.default.urlencoded({ limit: '50mb' }));
            this.app.use((0, cors_1.default)());
        });
    }
    routes() {
        return __awaiter(this, void 0, void 0, function* () {
            this.app.use('/enviar-mensaje', require('./routes/mensaje.route'));
            this.app.use('/bienvenida', require('./routes/mensaje.route'));
        });
    }
    sockets() {
        return __awaiter(this, void 0, void 0, function* () {
            socket_route_1.listaSocket.forEach(el => {
                this.serverIo.on(el.evento, el.funcion);
            });
        });
    }
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            this.httpServer.listen(this.port, () => {
                return console.log(`server is listening on ${this.port}`);
            });
        });
    }
}
exports.Servidor = Servidor;
