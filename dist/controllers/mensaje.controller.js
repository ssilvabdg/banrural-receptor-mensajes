"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const encript_1 = require("../encript");
const log_controller_1 = __importDefault(require("./log.controller"));
var component = log_controller_1.default.component;
var status = log_controller_1.default.status;
const mensaje = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const socket = req.app.get('socketIo');
    const axios = require('axios');
    const desencriptar = new encript_1.Encript();
    const varEncriptado = `${process.env.Encriptado}`;
    var reqEntrante = req.body;
    var auxEnvio;
    try {
        console.log("**************************************MENSAJE ENTRANTE************************************* ");
        console.log(req.body);
        console.log("******************************************************************************************* ");
        if (varEncriptado === '1') {
            reqEntrante = desencriptar.decodifi(req.body.d);
        }
        const { sender } = reqEntrante;
        var userTemp = reqEntrante.id.split("-");
        var pUsuario = userTemp[1];
        const sockets = yield socket.in("dispositivosConectados").allSockets();
        var conectado = false;
        sockets.forEach(element => {
            if (sender.socket == element) {
                conectado = true;
            }
        });
        if (conectado) {
            res.status(200).json({
                codigo: 0,
                mensaje: `Usted identifico al usuario ${sender.recipient} y reenvio la peticion`
            });
            if (varEncriptado === '1') {
                auxEnvio = JSON.stringify(reqEntrante);
                auxEnvio = `{\"message\":${auxEnvio}}`;
                auxEnvio = JSON.parse(auxEnvio);
                auxEnvio = desencriptar.codifi(auxEnvio);
                socket.emit(`mensaje-${sender.recipient}`, { d: auxEnvio });
            }
            else {
                socket.emit(`mensaje-${sender.recipient}`, { message: req.body });
            }
            console.log("**************************************MENSAJE ENVIADO************************************** ");
            console.log(req.body);
            console.log("******************************************************************************************* ");
        }
        else {
            res.status(400).json({
                codigo: 4,
                mensaje: `El usuario no se encuentra conectado`
            });
            log_controller_1.default.escribirBitacoraLog(pUsuario, component.socket, `NO SE PUDO IDENTIFICAR AL USUARIO ${sender.socket}`, status.fallido);
            console.log("NO SE PUDO IDENTIFICAR AL USUARIO QUE SE ENVIO COMO CONECTADO");
            var data = {};
            var aux2 = sender.id.split("-");
            if (varEncriptado === '1') {
                auxEnvio = JSON.stringify(reqEntrante);
                auxEnvio = `{\"message\":${auxEnvio}}`;
                auxEnvio = JSON.parse(auxEnvio);
                auxEnvio = desencriptar.codifi(auxEnvio);
                data = {
                    pUsuario: aux2[1],
                    pIdDispositivo: sender.recipient,
                    pMensajes: req.body.d
                };
            }
            else {
                const encode = Buffer.from(JSON.stringify(req.body)).toString('base64');
                data = {
                    pUsuario: aux2[1],
                    pIdDispositivo: sender.recipient,
                    pMensajes: encode
                };
            }
            try {
                var capamedia = require('../middlewares/capamedia')();
                capamedia.consultarBanrural(req.body, 851).then((res) => {
                    console.log("ESTE ES EL DATA", data);
                    if (res.eMensaje[0].pCodigo == 0) {
                        console.log("EL MENSAJE SE HA GUARDADO EN LA COLA DE MENSAJES");
                        log_controller_1.default.escribirBitacoraLog(pUsuario, component.apiCola, "EL MENSAJE SE HA GUARDADO EN LA COLA DE MENSAJES", status.exitoso);
                    }
                    else {
                        console.log("NO SE PUDO GUARDAR EL MENSAJE EN LA COLA DE MENSAJES");
                        log_controller_1.default.escribirBitacoraLog(pUsuario, component.apiCola, "NO SE PUDO GUARDAR EL MENSAJE EN LA COLA DE MENSAJES RECHAZADO POR SERVIDOR", status.fallido);
                    }
                }).catch((err) => {
                    console.log("NO SE PUDO GUARDAR EL MENSAJE EN LA COLA DE MENSAJES DEBIDO A ", err);
                    log_controller_1.default.escribirBitacoraLog(pUsuario, component.apiCola, "NO SE PUDO GUARDAR EL MENSAJE EN LA COLA DE MENSAJES DEBIDO A UN ERROR", status.fallido);
                });
            }
            catch (err) {
                console.log("NO SE PUDO GUARDAR EL MENSAJE EN LA COLA DE MENSAJES DEBIDO A ", err);
                log_controller_1.default.escribirBitacoraLog(pUsuario, component.apiCola, "NO SE PUDO GUARDAR EL MENSAJE EN LA COLA DE MENSAJES DEBIDO A UNA EXCEPCIÓN ", status.fallido);
            }
        }
    }
    catch (err) {
        res.status(400).json({
            codigo: 4,
            mensaje: `No se pudo realizar el reenvio de la peticion`
        });
        log_controller_1.default.escribirBitacoraLog(pUsuario, component.socket, "OCURRIO UNA EXCEPCIÓN AL INTENTAR REENVIAR EL MENSAJE", status.fallido);
        console.log('OCURRIO EL SIGUIENTE ERROR AL MOMENTO DE REENVIAR EL MENSAJE ', err);
    }
});
const mensaje2 = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        res.status(200).json({
            codigo: 0,
            mensaje: `Bienvenido al receptor de mensajes`
        });
    }
    catch (err) {
        res.status(400).json({
            codigo: 4,
            mensaje: `Ocurrio un error enviando la bienvenida`
        });
    }
});
const Ctrl = {
    mensaje,
    mensaje2
};
exports.default = Ctrl;
