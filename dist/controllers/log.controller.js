"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const escribirBitacoraLog = (usuario, componente, descripcion, status) => {
    const axios = require('axios');
    const fechaActual = new Date();
    var data = {
        pUsuario: usuario,
        pFechaHora: fechaActual,
        pComponente: componente,
        pDescripcion: descripcion,
        pStatus: status
    };
    console.log("datos enviados", data);
    var capamedia = require('../middlewares/capamedia')();
    capamedia.consultarBanrural(data, 855).then((res) => {
        console.log("SE GUARDO EL REGISTRO EN LA BITACORA", res);
    }).catch((err) => {
        console.log("OCURRIO UN PROBLEMA INSERTANDO EL REGISTRO EN LA BITACORA", err);
    });
};
var component;
(function (component) {
    component["socket"] = "socket IO";
    component["iaISOFT"] = "IA ISOFT";
    component["apiReceptor"] = "Api receptor";
    component["apiCola"] = "API cola de mensajes";
})(component || (component = {}));
var status;
(function (status) {
    status["exitoso"] = "200";
    status["fallido"] = "400";
})(status || (status = {}));
const log = { escribirBitacoraLog, component, status };
exports.default = log;
