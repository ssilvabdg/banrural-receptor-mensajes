"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Encript = void 0;
const crypto_js_1 = __importDefault(require("crypto-js"));
class Encript {
    constructor() {
        this.return_ = 'L84NC4M-SOIEIUSUDJADODROUlVSNE-29dixEOP9-7_P39392KDLKjdsiaspjdjisSEIOJMVNDKlskdkd';
        this.let_ = 'w4NE5DNE0w-djksli39djsdlcjsldj9-lsldkdkdk39KDKSDLOEñlsoldoeow03odfvjsOSOS0EOSPodosp';
        this.number_ = '-Vsasa83829K/VkkxMjAxN19S-RUR4/RUEO/ROC3mcmckdksiIOEOPSKDLSKSklmckdskdslkdskdeooweo';
        this.string_ = 'IDSI99kd0l-0918384N93029jdols.SI.0VI1201OEWOOWEPOEWOPCldslñlldsñldslsdldlsoeoewpolñsl';
    }
    codifi(parametros) {
        let json_envio = JSON.stringify(parametros);
        let vStringB64 = this.mesEncECBPKCS7(json_envio);
        let encriptado = vStringB64.toString(crypto_js_1.default.enc.Utf8);
        return encriptado;
    }
    decodifi(parametros) {
        let data = parametros;
        let respuesta_raw = data;
        let respuesta_b64 = '';
        respuesta_b64 = this.mesDecECBPKCS7(data);
        try {
            let json_respuesta = JSON.parse(respuesta_b64.toString());
            return json_respuesta;
        }
        catch (err) {
            console.log(err);
            return '';
        }
    }
    mesDecECBPKCS7(cod) {
        try {
            let kv = this.string_.substring(16, 19) +
                this.number_.substring(26, 30) + this.return_.substring(0, 7) +
                this.string_.substring(33, 40) + this.return_.substring(41, 44) +
                this.string_.substring(16, 19) + this.number_.substring(26, 30) + this.return_.substring(0, 1);
            //let kbyte = CryptoJS.enc.Base64.parse(kv);
            cod = cod.replace(/(\r\n|\n|\r)/gm, '');
            //datos encriptados como un arreglo de bytes
            let jsDec = crypto_js_1.default.AES.decrypt(cod, kv, {
                mode: crypto_js_1.default.mode.ECB,
                padding: crypto_js_1.default.pad.Pkcs7
            });
            //datos encriptados como string
            let jsDecString = jsDec.toString(crypto_js_1.default.enc.Utf8);
            return jsDecString;
        }
        catch (error) {
            console.log("Error al decodificar el mensaje:", error);
            return '';
        }
    }
    mesEncECBPKCS7(jsn) {
        try {
            let kv = this.string_.substring(16, 19) +
                this.number_.substring(26, 30) + this.return_.substring(0, 7) +
                this.string_.substring(33, 40) + this.return_.substring(41, 44) +
                this.string_.substring(16, 19) + this.number_.substring(26, 30) + this.return_.substring(0, 1);
            let jsonEnc = crypto_js_1.default.AES.encrypt(jsn, kv, { mode: crypto_js_1.default.mode.ECB, padding: crypto_js_1.default.pad.Pkcs7 }).toString();
            return jsonEnc;
        }
        catch (error) {
            console.log(error);
            return '';
        }
    }
}
exports.Encript = Encript;
